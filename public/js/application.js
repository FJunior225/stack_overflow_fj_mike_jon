$(document).ready(function() {

// FJ JS FOR USERS
  $('.edit-wrapper').on('click', '#edit-users', function(event){
    event.preventDefault();
    $.ajax({
      url: $(event.target).attr('href'),
      type: 'GET'
    })
    .done(function(response) {
      $('#delete-user').hide();
      $('#edit-users').hide();
      $('.edit-wrapper').append(response);
    })
    .fail(function(error) {
      console.log("error");
    });
  });

  $('.edit-wrapper').on('submit', '#edit-user-form', function(event){
    event.preventDefault();
    $.ajax({
      url: $(event.target).attr('action'),
      type: $(event.target).attr('method'),
      data: $(event.target).serialize()
    })
    .done(function(response) {
      $('#user-partial').html(response);
      $('#delete-user').show();
      $('#edit-users').show();
      $('.edit-sub-form').hide();
      console.log('success');
    })
    .fail(function(error) {
      console.log("error");
    });
  });

// FJ JS FOR UPVOTE & DOWNVOTE
  $('.question-container').on('click', '.upvote-button', function(event){
    event.preventDefault();
    var $target = $(event.target)
    $.ajax({
      url: $(event.target).attr('href'),
      type: 'post',
      dataType: 'json'
    })
    .done(function(response) {
    $target.closest('.question_summary').find('.question_vote_count').text(response.vote_counter)
    })
    .fail(function() {
      console.log("error");
    });

  });
  $('.question-container').on('click', '.downvote-button', function(event){
    event.preventDefault();
    var $target = $(event.target)
    $.ajax({
      url: $(event.target).attr('href'),
      type: 'post',
      dataType: 'json'
    })
    .done(function(response) {
    $target.closest('.question_summary').find('.question_vote_count').text(response.vote_counter)
    })
    .fail(function() {
      console.log("error");
    });
})


// FJ JS FOR UPVOTE & DOWNVOTE INSIDE A SPECIFIC QUESTION
  $('.question_detailed').on('click', '.upvote-button', function(event){
    event.preventDefault();
    var $target = $(event.target)
    $.ajax({
      url: $(event.target).attr('href'),
      type: 'post',
      dataType: 'json'
    })
    .done(function(response) {
    $('.specific-question_vote_count').text(response.vote_counter)
    })
    .fail(function() {
      console.log("error");
    });

  });
  $('.question_detailed').on('click', '.downvote-button', function(event){
    event.preventDefault();
    var $target = $(event.target)
    $.ajax({
      url: $(event.target).attr('href'),
      type: 'post',
      dataType: 'json'
    })
    .done(function(response) {
    $('.specific-question_vote_count').text(response.vote_counter)
    })
    .fail(function() {
      console.log("error");
    });
})

// FJ JS FOR UPVOTE & DOWNVOTE FOR A COMMENT
  $('.comment-vote').on('click', '.upvote-button', function(event){
    event.preventDefault();
    var $target = $(event.target)
    $.ajax({
      url: $(event.target).attr('href'),
      type: 'post',
      dataType: 'json'
    })
    .done(function(response) {
    $target.closest('.comment').find('.comment_vote_count').text(response.vote_counter)
    })
    .fail(function() {
      console.log("error");
    });

  });
  $('.comment-vote').on('click', '.downvote-button', function(event){
    event.preventDefault();
    var $target = $(event.target)
    $.ajax({
      url: $(event.target).attr('href'),
      type: 'post',
      dataType: 'json'
    })
    .done(function(response) {
    $target.closest('.comment').find('.comment_vote_count').text(response.vote_counter)
    })
    .fail(function() {
      console.log("error");
    });
})
// JON JS FOR QUESTIONS
  $("#ask_question").on("click", function(event){
    event.preventDefault();
    $.ajax({
      url: $(event.target).attr("href")
    }).done(function(response){
      $('#ask_question').replaceWith(response);
      // $("#new_question_form_holder").replaceWith(response)
    }).fail(function(error){
      console.log("ERROR")
    });
  });

  $(".ask-parent").on("submit", "#new_question_form", function(event){
    event.preventDefault();
    $.ajax({
      url: $(event.target).attr("action"),
      method: $(event.target).attr("method"),
      data: $(event.target).serialize(),
      dataType: "html"
    }).done(function(response){
      $('#question-form').hide();
      $('.question-container').prepend(response)
    }).fail(function(error){
      console.log("ERROR")
    })
  });

  // MG JS for marking best answer
  $('.best-button').on('click', function(event){
      event.preventDefault()
      $target = $(event.target)
      $.ajax({
        url: $target.attr('href'),
        type: 'post',
        dataType: 'html'
      }).done(function(response){
        $target.closest('.comment').prepend(response)
        $('.best-button').hide()
      }).fail(function(){
        console.log("error");
      });
  })

  // MG JS for loading answer comments
  $('.comment').on('mouseenter', function(event){
    event.preventDefault();
    $target = $(event.target).closest('.comment').find('.comment_selector')
    $.ajax({
      url: $target.attr('href') + "/comments",
      type: 'get',
      dataType: 'html'
    }).done(function(response){
      $target.closest('.comment').append(response).unbind()
    }).fail(function(){
      console.log("error");
    })
  });

// MG JS form for answering a question
  $('.answer_question').on('click', function(event){
    event.preventDefault();
    $target = $(event.target)
    $.ajax({
      type: 'get',
      url: $target.attr('href')
    }).done(function(response){
      $target.closest('.question_detailed').append(response);
    }).fail(function(error){
      console.log('Error')
    });
  });

// MG JS for submiting an answer to a question
  $('.answer_submit').on('submit', '.new_answer_form', function(event){
    event.preventDefault();
    $target = $(event.target)
    $.ajax({
      url: $target.attr('action'),
      method: $target.attr('method'),
      data: $target.serialize(),
      dataType: 'html'
    }).done(function(response){
      $('#new_answer_form').hide();
      $('.question-container').prepend(response)
    }).fail(function(error){
      console.log('error')
    })
  });

// MG JS form for commenting on an answer

  $('.comment_new').on('click', function(event){
    event.preventDefault();
    $target = $(event.target)
    $.ajax({
      type: 'get',
      url: $target.attr('href')
    }).done(function(response){
      $target.closest('.comment').append(response);
    }).fail(function(error){
      console.log('Error')
    });
  });

// MG JS for submiting a comment to an answer
  $('.comment').on('submit', '.comment_submit', function(event){
    event.preventDefault();
    $target = $(event.target)
      debugger;
    $.ajax({
      url: $target.attr('action'),
      method: $target.attr('method'),
      data: $target.serialize(),
      dataType: 'html'
    }).done(function(response){
      $('.comment_submit').hide();
      $target.last('.sub_comment').append(response)
    }).fail(function(error){
      console.log('error')
    })
  });

});
