users = 5
questions = 15
tags = 10

users.times do
  User.create(email: Faker::Internet.email, username: Faker::Name.name, password: "password")
end

questions.times do
  Question.create(title: "How do I " + Faker::Hacker.adjective + "?", question_body: Faker::Hacker.say_something_smart, user_id: rand(1..5))
end

tags.times do
  Tag.create(name: Faker::Color.color_name)
end

User.create(email: "bob@gmail.com", username: "bob", password: "password")

Question.create(title: "I'm confused", question_body: "Please help me!!!", user_id: 1)

Comment.create(comment_body: "Comment number 1",user_id: 1, commentable_id: 1, commentable_type: "Question")

Comment.create(comment_body: "Comment number 2",user_id: 1, commentable_id: 1, commentable_type: "Comment")

Comment.create(comment_body: "Comment number 3",user_id: 1, commentable_id: 1, commentable_type: "Comment")

Tag.create(name: "confused")

Vote.create(vote_value: 1, voteable_id: 1, voteable_type: "Question", user_id: 1)

Vote.create(vote_value: 1, voteable_id: 1, voteable_type: "Comment", user_id: 1)

Vote.create(vote_value: -1, voteable_id: 1, voteable_type: "Question", user_id: 1)
