class AddQuestions < ActiveRecord::Migration
  def change
    create_table   :questions do |t|
      t.string     :title, null: false
      t.text       :question_body, null: false
      t.integer    :view_count, default: 0
      t.references :user, null: false, index: true

      t.timestamps null: false
    end
  end
end
