class AddQuestionsTags < ActiveRecord::Migration
  def change
    create_table   :questions_tags do |t|
      t.references :question, index: true, null: false
      t.references :tag, index: true, null: false

      t.timestamps null: false
    end
  end
end
