class AddComments < ActiveRecord::Migration
  def change
    create_table   :comments do |t|
      t.boolean    :best_answer, default: false
      t.text       :comment_body, null: false
      t.references :commentable, polymorphic: true, index: true
      t.references :user, null: false, index: true

      t.timestamps null: false
    end
  end
end
