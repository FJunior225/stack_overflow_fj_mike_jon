class AddVotes < ActiveRecord::Migration
  def change
    create_table   :votes do |t|
      t.integer    :vote_value, default: 0
      t.references :voteable, polymorphic: true, index: true, null: false
      t.references :user, null: false

      t.timestamps null: false
    end
  end
end
