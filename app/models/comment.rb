class Comment < ActiveRecord::Base
  belongs_to :question
  belongs_to :user
  belongs_to :commentable, polymorphic: true
  has_many :votes, as: :voteable
  has_many :comments, as: :commentable


  def vote_count
    votes.sum(:vote_value)
  end
end
