class Question < ActiveRecord::Base
  has_many :votes, as: :voteable
  has_many :comments, as: :commentable
  has_and_belongs_to_many :tags
  belongs_to :user
  validates :title, :question_body, presence: true

def split_tags
  tags.map { |tag| tag.name}.join(" | ")
end

def vote_count
  votes.sum(:vote_value)
end

end
