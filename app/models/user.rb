class User < ActiveRecord::Base
  has_secure_password
  has_many :questions
  has_many :comments
  has_many :votes
  validates :email, :username, presence: true
  validates :username, uniqueness: true
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create
end
