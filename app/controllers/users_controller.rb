get '/users/new' do
  @user = User.new
  erb :'users/new'
end

post '/users' do
  @user = User.create(params[:user])
  if @user.save
    found_user = User.find_by(username: params[:user][:username])
    if found_user && found_user.authenticate(params[:user][:password])
      session[:user_id] = found_user.id
      redirect "/users/#{@user.id}"
    else
      erb :'/sessions/new'
    end
  else
    @errors = @user.errors.full_messages
    erb :'users/new'
  end
end

get '/users/:id' do
  @user = User.find(params[:id])
  halt(401, erb(:unauthorized)) unless current_user == @user
  erb :'users/show'
end

get '/users/:id/edit' do
  @user = User.find(params[:id])
  halt(401, erb(:unauthorized)) unless current_user == @user
  if request.xhr?
    erb :'/users/edit', layout: false
  else
    erb :'users/show'
  end
end

put '/users/:id' do
  @user = User.find(params[:id])
  halt(401, erb(:unauthorized)) unless current_user == @user
  @user.assign_attributes(params[:user])
  if @user.save
    if request.xhr?
      erb :'users/_user', locals: {user: @user}, layout: false
    else
      erb :'/users/edit'
    end
  else
    @errors = @user.errors.full_messages
    erb :'/users/edit'
  end
end

delete '/users/:id' do
  user = User.find(params[:id])
  user.destroy
  redirect '/questions'
end