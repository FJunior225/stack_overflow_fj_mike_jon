get '/login' do
  erb :'/sessions/new'
end

post '/login' do
  found_user = User.find_by(username: params[:user][:username])
  if found_user && found_user.authenticate(params[:user][:password])
    session[:user_id] = found_user.id
    redirect "/users/#{found_user.id}"
  else
    @errors = ["Wrong username or password"]
    erb :'/sessions/new'
  end
end

delete '/logout' do
  session.clear
  redirect '/questions'
end
