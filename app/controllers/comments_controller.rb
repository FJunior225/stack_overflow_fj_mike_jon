get '/questions/:id/comments' do
  @comments = Question.find(params[:id]).comments
  @question = Question.find(params[:id])
  if request.xhr?
    erb :_question, locals: {comment: @comments}, layout: false
  else
    erb :'/comments/index'
  end
end

get '/questions/:id/comments/new' do
  if request.xhr?
    erb :'_question_answer_form', layout: false
  else
    erb :'/comments/new'
  end
end

post '/questions/:id/comments' do
  @comment = Comment.new(commentable_id: params[:id], user_id: current_user.id, commentable_type: "Question", comment_body: params[:comment_text])
  @comment.save
  redirect "/questions/#{params[:id]}"
end

get '/questions/:id/comments/:id' do
  if request.xhr?
    @answer = Comment.find(:id)
    @sub_comments = Comment.find(@answer.id).comments
    erb :_answer_comments, locals:{sub_comments: @sub_comments}, layout: false
  else
    @comment = Comment.find(params[:id])
    erb :'/comments/edit'
  end
end

put '/questions/:id/comments/:id' do
  @comment = Comment.find(params[:captures][1])
  @comment.comment_body = params[:comment_text]
  @comment.save
  redirect "/questions/#{params[:captures][0]}"
end

delete '/questions/:id/comments/:id' do
  Comment.delete(params[:captures][1])
  redirect "/questions/#{params[:captures][0]}"
end

post '/questions/:id/comments/:id/best' do
  @comment = Comment.find(params[:captures][1])
  @comment.best_answer = true
  @comment.save
  if request.xhr?
    erb :'_best_answer', layout: false
  end
end
