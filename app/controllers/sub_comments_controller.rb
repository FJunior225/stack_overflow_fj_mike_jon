get '/questions/:id/comments/:id/comments' do
  @comments = Comment.find(params[:captures][1]).comments
  if request.xhr?
    erb :'_comments_to_answers', locals:{comments: @comments}, layout: false
  else
    erb :'/comments/index'
  end
end

get '/questions/:id/comments/:id/comments/new' do
  if request.xhr?
    erb :'_answer_comment_form', layout: false
  else
    erb :'/sub_comments/new'
  end
end

post '/questions/:id/comments/:id/comments' do
  @comment = Comment.create(commentable_id: params[:id], user_id: current_user.id, commentable_type: "Comment", comment_body: params[:comment_text])
  if request.xhr?
    erb :'/_answer_comment', locals:{comment: @comment}, layout: false
  else
    redirect "/questions/#{params[:captures][0]}"
  end
end
