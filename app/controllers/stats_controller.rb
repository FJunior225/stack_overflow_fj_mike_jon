post '/questions/:id/upvote' do
  @question = Question.find(params[:id])
  halt(401, erb(:unauthorized)) unless logged_in?
  vote = @question.votes.new(vote_value: 1, user_id: current_user.id)
  if request.xhr?
    if vote.save
      { vote_counter: @question.vote_count }.to_json
    else
      halt(401)
    end
  else
    redirect '/questions'
  end
end

post '/questions/:id/downvote' do
  @question = Question.find(params[:id])
  halt(401, erb(:unauthorized)) unless logged_in?
  vote = @question.votes.new(vote_value: -1, user_id: current_user.id)
  if request.xhr?
    if vote.save
      { vote_counter: @question.vote_count }.to_json
    else
      halt(401)
    end
  else
    redirect '/questions'
  end
end

post '/questions/:id/comments/:id/upvote' do
  # @question = Question.find(params[:id])
  @comment = Comment.find(params[:captures][1])
  halt(401, erb(:unauthorized)) unless logged_in?
  vote = @comment.votes.new(vote_value: 1, user_id: current_user.id)
  if request.xhr?
    if vote.save
      { vote_counter: @comment.vote_count }.to_json
    else
      halt(401)
    end
  else
    redirect "/questions/#{params[:captures][0]}"
  end
end

post '/questions/:id/comments/:id/downvote' do
  # @question = Question.find(params[:id])
  @comment = Comment.find(params[:captures][1])
  halt(401, erb(:unauthorized)) unless logged_in?
  vote = @comment.votes.new(vote_value: -1, user_id: current_user.id)
  if request.xhr?
    if vote.save
      { vote_counter: @comment.vote_count }.to_json
    else
      halt(401)
    end
  else
    redirect "/questions/#{params[:captures][0]}"
  end
end