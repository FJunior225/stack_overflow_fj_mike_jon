get '/' do
  redirect '/questions'
end

get '/questions' do
  @question = Question.find_by(params[:question])
  @all_questions = Question.order(created_at: :desc)
  erb :'/questions/index'
end

get '/questions/new' do
  if logged_in?
    if request.xhr?
      erb :'/questions/new', layout: false
    else
      erb :'/questions/new'
    end
  else
    halt(401, erb(:unauthorized))
  end
end

post '/questions' do
  @question = Question.new(params[:question])
  @tags = params[:tag][:name].split(", ")
  @tags.each { |tag| @question.tags << Tag.find_or_create_by(name: tag) }
  @question.user_id = current_user.id
  if @question.save
    if request.xhr?
      erb :'_question_summary', locals: { question: @question }, layout: false
    else
      @questions = Question.all
      erb :'/questions/index'
    end
  else
    @errors = @question.errors.full_messages
  end
end

get '/questions/:id' do
  @question = Question.find(params[:id])
  @tags = @question.split_tags
  @comments = @question.comments
  @question.view_count += 1
  @question.save
  erb :'questions/show'
end

get '/questions/:id/edit' do
  @question = Question.find(params[:id])
  @tags = @question.tags
  if logged_in? && current_user.id == @question.user_id
    erb :'questions/edit'
  else
    halt(401, erb(:unauthorized))
  end
end

put '/questions/:id' do
  @question = Question.find(params[:id])
  @question.assign_attributes(params[:question])
  # @tag = @question.tags.each { |tag| tag }
  # @tags = @tag.each { |tag| tag.assign_attributes(params[:tags])
  #   @question << @tags }
  if @question.save
    redirect "/questions/#{@question.id}"
  else
    @errors = @question.errors.full_messages
    erb :"/questions/edit"
  end
end

delete '/questions/:id' do
  question = Question.find(params[:id])
  if logged_in? && current_user.id == @question.user_id
    question.destroy
    redirect '/questions'
  else
    halt(401, erb(:unauthorized))
  end
end
